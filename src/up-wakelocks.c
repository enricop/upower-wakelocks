/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2008 Richard Hughes <richard@hughsie.com>
 *               2010 Alex Murray <murray.alex@gmail.com>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <glib/gi18n.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>

#include "up-wakelocks.h"
#include "up-marshal.h"
#include "up-daemon.h"
#include "up-wakelocks-glue.h"
#include "up-types.h"

static void     up_wakelocks_finalize   (GObject	*object);

#define UP_WAKELOCKS_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), UP_TYPE_WAKELOCKS, UpWakelocksPrivate))

struct UpWakelocksPrivate
{
    gint			 fd_acquire;
    gint			 fd_release;
    gchar			 *active_wakelocks;
    DBusGConnection     *connection;
};

G_DEFINE_TYPE (UpWakelocks, up_wakelocks, G_TYPE_OBJECT)

/**
 * up_wakelocks_write:
 **/
static gboolean
up_wakelocks_write (UpWakelocks *wakelocks, gchar *resource, gboolean mode)
{
	gint retval;
	gint length;
	gboolean ret = TRUE;

    /* write new values to wakelocks */
    if (mode ? wakelocks->priv->fd_acquire : wakelocks->priv->fd_release < 0) {
        g_warning ("cannot write to wakelocks as file not open");
        ret = FALSE;
        goto out;
    }

    length = strlen (resource);

	/* write to file */
    lseek (mode ? wakelocks->priv->fd_acquire : wakelocks->priv->fd_release,
           0, SEEK_SET);
    retval = write (mode ? wakelocks->priv->fd_acquire : wakelocks->priv->fd_release,
                    resource, length);
	if (retval != length) {
        g_warning ("writing '%s' to device failed", resource);
		ret = FALSE;
		goto out;
	}

out:
	return ret;
}

/**
 * up_wakelocks_get_active:
 *
 * Gets the current active wakelocks
 **/
gboolean
up_wakelocks_get_active (UpWakelocks *wakelocks, gchar **list, GError **error)
{
    g_return_val_if_fail (list != NULL, FALSE);
    *list = g_strdup(wakelocks->priv->active_wakelocks);
	return TRUE;
}

/**
 * up_wakelocks_acquire:
 **/
gboolean
up_wakelocks_acquire (UpWakelocks *wakelocks, char *res, GError **error)
{
	gboolean ret = FALSE;

    g_debug ("acquiring resource %s", res);
    ret = up_wakelocks_write(wakelocks, res, TRUE);

	if (!ret) {
        *error = g_error_new (UP_DAEMON_ERROR, UP_DAEMON_ERROR_GENERAL, "error writing brightness %s", res);
	}
	return ret;
}

/**
 * up_wakelocks_release:
 **/
gboolean
up_wakelocks_release (UpWakelocks *wakelocks, char *res, GError **error)
{
    gboolean ret = FALSE;

    g_debug ("releasing resource %s", res);
    ret = up_wakelocks_write(wakelocks, res, FALSE);

    if (!ret) {
        *error = g_error_new (UP_DAEMON_ERROR, UP_DAEMON_ERROR_GENERAL, "error writing brightness %s", res);
    }
    return ret;
}

/**
 * up_wakelocks_class_init:
 **/
static void
up_wakelocks_class_init (UpWakelocksClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
    object_class->finalize = up_wakelocks_finalize;

//	signals [BRIGHTNESS_CHANGED] =
//		g_signal_new ("brightness-changed",
//			      G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST,
//                  G_STRUCT_OFFSET (UpWakelocksClass, brightness_changed),
//			      NULL, NULL, g_cclosure_marshal_VOID__INT,
//			      G_TYPE_NONE, 1, G_TYPE_INT);

	/* introspection */
    dbus_g_object_type_install_info (UP_TYPE_WAKELOCKS, &dbus_glib_up_wakelocks_object_info);

    g_type_class_add_private (klass, sizeof (UpWakelocksPrivate));
}

/**
 * up_wakelocks_find:
 **/
static gboolean
up_wakelocks_find (UpWakelocks *wakelocks)
{
	gboolean ret;
	gboolean found = FALSE;
	GDir *dir;
    gchar *dir_path = NULL;
    gchar *acquire_path = NULL;
    gchar *release_path = NULL;
	gchar *buf_max = NULL;
	GError *error = NULL;

    wakelocks->priv->fd_release = -1;
    wakelocks->priv->fd_acquire = -1;

	/* open directory */
    dir = g_dir_open ("/sys/power", 0, &error);
	if (dir == NULL) {
		g_warning ("failed to get directory: %s", error->message);
		g_error_free (error);
		goto out;
	}

    dir_path = g_dir_read_name (dir);
	/* nothing found */
	if (dir_path == NULL)
		goto out;

    acquire_path = g_build_filename ("/sys/power", "wake_lock", NULL);
    release_path = g_build_filename ("/sys/power", "wake_unlock", NULL);

	/* read max brightness */
    ret = g_file_get_contents (acquire_path, &buf_max, NULL, &error);
	if (!ret) {
        g_warning ("failed to get active wakelocks: %s", error->message);
		g_error_free (error);
		goto out;
	}
    wakelocks->priv->active_wakelocks = g_strdup (buf_max);
    if (wakelocks->priv->active_wakelocks == 0) {
        g_warning ("failed to convert read active wakelocks: %s", buf_max);
		goto out;
	}

	/* open the file for writing */
    wakelocks->priv->fd_acquire = open (acquire_path, O_RDWR);
    if (wakelocks->priv->fd_acquire < 0)
		goto out;
    /* open the file for writing */
    wakelocks->priv->fd_release = open (release_path, O_RDWR);
    if (wakelocks->priv->fd_release < 0)
        goto out;

	/* success */
	found = TRUE;
out:
	if (dir != NULL)
		g_dir_close (dir);

    g_free (release_path);
    g_free (acquire_path);

	return found;
}

/**
 * up_wakelocks_init:
 **/
static void
up_wakelocks_init (UpWakelocks *wakelocks)
{
	GError *error = NULL;

    wakelocks->priv = UP_WAKELOCKS_GET_PRIVATE (wakelocks);

    /* find wakelocks in sysfs */
    if (!up_wakelocks_find (wakelocks)) {
        g_debug ("cannot find wakelocks in sysfs");
		return;
	}

    wakelocks->priv->connection = dbus_g_bus_get (DBUS_BUS_SYSTEM, &error);
	if (error != NULL) {
		g_warning ("Cannot connect to bus: %s", error->message);
        g_error_free (error);
		return;
	}

	/* register on the bus */
    dbus_g_connection_register_g_object (wakelocks->priv->connection, "/org/freedesktop/UPower/Wakelocks", G_OBJECT (wakelocks));

}

/**
 * up_wakelocks_finalize:
 **/
static void
up_wakelocks_finalize (GObject *object)
{
    UpWakelocks *wakelocks;

	g_return_if_fail (object != NULL);
    g_return_if_fail (UP_IS_WAKELOCKS (object));

    wakelocks = UP_WAKELOCKS (object);
    wakelocks->priv = UP_WAKELOCKS_GET_PRIVATE (wakelocks);

	/* close file */
    close (wakelocks->priv->fd_release);
    close (wakelocks->priv->fd_acquire);

    G_OBJECT_CLASS (up_wakelocks_parent_class)->finalize (object);
}

/**
 * up_wakelocks_new:
 **/
UpWakelocks *
up_wakelocks_new (void)
{
    UpWakelocks *wakelocks;
    wakelocks = g_object_new (UP_TYPE_WAKELOCKS, NULL);
    return UP_WAKELOCKS (wakelocks);
}

